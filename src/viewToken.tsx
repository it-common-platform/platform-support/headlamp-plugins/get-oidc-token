import { InlineIcon } from '@iconify/react';
import { SectionBox } from '@kinvolk/headlamp-plugin/lib/CommonComponents';
import { Button, IconButton, InputAdornment, Snackbar, TextField } from '@mui/material';
import Stack from '@mui/material/Stack';
import React, { useEffect, useRef, useState } from 'react';
import K8s from '@kinvolk/headlamp-plugin/lib/K8s';

export function ViewToken() {
  const [token, setToken] = useState('{}');
  const [open, setOpen] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const textAreaRef = useRef(null);

  useEffect(() => {
    const storedTokens = localStorage.getItem('tokens');
    if (storedTokens) {
      setToken(storedTokens || '{}');
    }
    if (!window.localStorage) {
      console.log('Error: No window');
    }
  }, []);

  const handleClick = async copyMe => {
    await navigator.clipboard.writeText(copyMe);
    setOpen(true);
  };

  const handleClickShowPassword = () => setShowPassword(show => !show);
  const handleMouseDownPassword = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
  };

  const [versionMajorInfo, setVersionMajorInfo] = useState(null);
  const [versionMinorInfo, setVersionMinorInfo] = useState(null);
  const [versionMinorDecrInfo, setVersionMinorDecrInfo] = useState(null);
  const [versionMinorPlusInfo, setVersionMinorPlusInfo] = useState(null);
  useEffect(() => {
    async function fetchKubernetes(){
      const response = await K8s.getVersion();
      if (!K8s){
        console.error("Doesn't exist");
      }

      console.log('Kubernetes Version: ', response);
      const match = response.minor.match((/^(\d+(\.\d+)*)([\+\-\*]*)$/));
      setVersionMajorInfo(response.major);

      var regVersion = Number(match[1]);
      setVersionMinorInfo(regVersion);
    
      var decrVersion = Number(match[1]);
      setVersionMinorDecrInfo(--decrVersion);

      var additionVersion = Number(match[1]);
      setVersionMinorPlusInfo(++additionVersion);

    }

    fetchKubernetes();
  }, []);

  const parsedToken = JSON.parse(token).main ?? '{}';
  const identity =
    atob(parsedToken.split('.')[1] ?? 'eyJlbWFpbCI6Im5vX3Jlc3VsdEB2dC5lZHUifQo=') ?? '{}';
  const email = JSON.parse(identity)?.email ?? '';
  const user = email.split('@')[0] ?? '';

  let url = 'https://headlamp.test.local.itcp.cloud.vt.edu/c/main';
  if (window.location.href.startsWith('https://')) {
    url = window.location.href;
  }

  const baseUrl = url?.split('/')[2] ?? ''; // Example baseUrl: headlamp.dvlp.aws.itcp.cloud.vt.edu
  const clusterUrl = 'https://' + baseUrl.replace('headlamp', 'k8s-api');
  const location = baseUrl.split('.')[2].replace('op', 'eksa') ?? 'undefined';
  const tier = baseUrl.split('.')[1] ?? 'undefined';
  const cluster = `${location}-${tier}-platform-cluster`;

  const context = `${user}-${cluster}`;
  const setCluster = `kubectl config set-cluster ${cluster} --server=${clusterUrl}`;
  const setContext = `kubectl config set-context ${context} --cluster=${cluster} --user=${context}`;
  const useContext = `kubectl config use-context ${context}`;
  const setCredentials = `kubectl config set-credentials ${context} --token="${parsedToken}"`;
  const allCommands = `${setCluster}; ${setContext}; ${useContext}; ${setCredentials}`;
  const contextAndCredentials = `${useContext}; ${setCredentials}`;

  return (
    <SectionBox title="Configure your Local CLI/Tools" textAlign="left" paddingTop={2}>
      <p>
        The following commands can be used to configure your local machine to query resources using
        Kubectl (Compatible Versions: {versionMajorInfo}.{versionMinorDecrInfo}, {versionMajorInfo}.{versionMinorInfo}, and {versionMajorInfo}.{versionMinorPlusInfo}) or other tools.
      </p>
      <h3>One-time setup</h3>
      <p>The following commands will need to be run only once, as the settings stay constant.</p>
      <Stack direction="column" gap={2}>
        <Stack direction="row" alignItems="center" gap={2}>
          <form>
            <TextField
              fullWidth
              variant="outlined"
              sx={{ m: 1, width: '80ch' }}
              ref={textAreaRef}
              value={setCluster}
              helperText="Define the cluster"
            />
          </form>
          <Button onClick={() => handleClick(setCluster)}>Copy</Button>
        </Stack>
        <Stack direction="row" alignItems="center" gap={2}>
          <form>
            <TextField
              fullWidth
              variant="outlined"
              sx={{ m: 1, width: '80ch' }}
              multiline
              ref={textAreaRef}
              value={setContext}
              helperText="Define the context"
            />
          </form>
          <Button onClick={() => handleClick(setContext)}>Copy</Button>
        </Stack>
      </Stack>
      <h3>More frequent setup</h3>
      <p>You will need to switch contexts to change clusters.</p>
      <p>
        Since the credential to access the cluster expires, you will need to frequently refresh it.
      </p>
      <Stack direction="column" gap={2}>
        <Stack direction="row" alignItems="center" gap={2}>
          <form>
            <TextField
              fullWidth
              variant="outlined"
              sx={{ m: 1, width: '80ch' }}
              ref={textAreaRef}
              value={useContext}
              helperText="Set the context to use"
            />
          </form>
          <Button onClick={() => handleClick(useContext)}>Copy</Button>
        </Stack>
        <Stack direction="row" alignItems="center" gap={2}>
          <form>
            <TextField
              fullWidth
              variant="outlined"
              sx={{ m: 1, width: '80ch' }}
              type={showPassword ? 'text' : 'password'}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      onClick={handleClickShowPassword}
                      onMouseDown={handleMouseDownPassword}
                      edge="end"
                      size="large"
                    >
                      {showPassword ? (
                        <InlineIcon icon="mdi:eye-off" />
                      ) : (
                        <InlineIcon icon="mdi:eye" />
                      )}
                    </IconButton>
                  </InputAdornment>
                ),
              }}
              ref={textAreaRef}
              value={setCredentials}
              helperText="Set the temporary token"
            />
          </form>
          <Button onClick={() => handleClick(setCredentials)}>Copy</Button>
        </Stack>
        <Stack direction="row" alignItems="center" gap={2}>
          <Button onClick={() => handleClick(allCommands)}>Copy All</Button>
          <Button onClick={() => handleClick(contextAndCredentials)}>Copy Context and Token</Button>
        </Stack>
      </Stack>
      <Snackbar
        open={open}
        onClose={() => setOpen(false)}
        autoHideDuration={2000}
        message="Copied to clipboard"
      />
    </SectionBox>
  );
}
