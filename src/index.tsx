import { registerRoute, registerSidebarEntry } from '@kinvolk/headlamp-plugin/lib';
import { ViewToken } from './viewToken';

registerSidebarEntry({
  parent: null,
  name: 'getToken',
  label: 'Get Token',
  url: '/getToken',
  icon: 'mdi:key',
});

registerRoute({
  path: '/getToken',
  sidebar: 'getToken',
  name: 'getToken',
  exact: true,
  component: () => <ViewToken />,
});
